class SearchBroadcastJob < ApplicationJob
  queue_as :default

  def perform(student)
    ActionCable.server.broadcast "search_channel",
      student
  end
end
