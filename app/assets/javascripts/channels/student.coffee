App.student = App.cable.subscriptions.create "StudentChannel",
  connected: ->

  disconnected: ->

  received: (data) ->
    $('.student').empty()
    $('.student').append('Họ tên: ' + data.name)
    $('.student').append('</br>')
    $('.student').append('MSSV: ' + data.mssv)
    $('.student').append('</br>')
    $('.student').append('Email: ' + data.email)
    $('.student').append('</br>')
    $('.student').append('Lớp: : ' + data.class_name)
    $('.student').append('</br>')
    $('.student').append('Ngày sinh: ' + data.date_of_birth)
    $('.student').append('</br>')
    $('.student').append('Quê quán: ' + data.address)

  search: ->
    @perform 'search'
