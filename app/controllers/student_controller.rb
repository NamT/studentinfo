class StudentController < ApplicationController
  def index
    @student = Student.find_by(mssv: params[:mssv])
    SearchBroadcastJob.perform_now(@student)
    render html: @student.name
  end
end
