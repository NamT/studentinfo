# Learn more: http://github.com/javan/whenever
require_relative "environment"
ENV.each { |k, v| env(k, v) }
set :environment, Rails.env
set :output, "#{path}/log/cron_log.log"

every 1.minute do
  rake "notification:create"
end
