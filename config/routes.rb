Rails.application.routes.draw do
  root to: "application#index"
  resources :student, only: [:index]
  mount ActionCable.server => '/cable'
end
