class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.string :mssv
      t.string :address
      t.date :date_of_birth
      t.string :class
      t.timestamps
    end
  end
end
