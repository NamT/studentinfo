# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.destroy_all
Student.create(name: "Vũ Thiệu Nam", mssv: "17020920", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "namthp99@gmail.com")
Student.create(name: "Trần Tuấn Ngọc", mssv: "17020956", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Công Phước", mssv: "17020964", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Thành Nam", mssv: "17020914", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Ngọc Nghĩa", mssv: "17020926", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Vĩnh An", mssv: "17020000", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Tiến Thành", mssv: "17020001", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Đặng Tú Quyên", mssv: "17020002", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Thị Tí", mssv: "17020003", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Hồng Phương", mssv: "17020004", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Nhật Nam", mssv: "17020005", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Lê Hồng Nhung", mssv: "17020005", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Quang Hải", mssv: "17020006", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Phan Đình Tùng", mssv: "17020007", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Phan Đình Phùng", mssv: "17020008", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")
Student.create(name: "Nguyễn Hoài An", mssv: "17020009", address: "Hai Phong", date_of_birth:"20-04-1999", class_name: "K62 CD", email: "exam@gmail.com")

