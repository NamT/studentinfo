namespace :notification do
  desc "notification"
  task create: :environment do
    Student.create(name: "example" + Time.now.to_i.to_s, 
                   mssv: Student.all.count + 1, 
                   address: "Hai Phong", 
                   date_of_birth: "20/04/1999", 
                   class_name: "K62 CD", 
                   email: "example" + Time.now.to_i.to_s + "@gmail.com")
  end
end
  